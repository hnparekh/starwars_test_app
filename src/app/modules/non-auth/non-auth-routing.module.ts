import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NonAuthLayoutComponent } from './non-auth-layout.component';
import { LandingPageComponent } from './components';
import { PersonDetailsComponent } from './components/person-details/person-details.component';
import { FavouritePeopleComponent } from './components/favourite-people/favourite-people.component';
import { ComicStoreComponent } from './components/comic-store/comic-store.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';

const routes: Routes = [
  {
    path: '', component: NonAuthLayoutComponent,
    children: [
      { path: '', component: LandingPageComponent },
      { path: 'person', component: PersonDetailsComponent },
      {
        path: 'favourite-people', component: FavouritePeopleComponent
      },
      {
        path: 'comic-stores', component: ComicStoreComponent
      },
      {
        path: 'search-results/:term', component: SearchResultsComponent
      }
    ]
  }
];

export const NonAuthRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
