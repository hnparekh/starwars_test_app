import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ComicStore } from 'src/app/models/people';
import { LoadingService } from 'src/app/core/services/loading.service';
declare var google: any;
var map;
var infowindow;

@Component({
  selector: 'app-comic-store',
  templateUrl: './comic-store.component.html',
  styleUrls: ['./comic-store.component.scss']
})
export class ComicStoreComponent implements OnInit {
  @ViewChild('map', { static: true }) mapElement: ElementRef;
  isAllowedCurrentLocation: boolean;
  stores: Array<ComicStore> = [];

  constructor(
    private loadingService: LoadingService
  ) { }

  ngOnInit() {
    this.isAllowedCurrentLocation = false;
    this.loadMap();
  }

  loadMap() {
    if (navigator.geolocation) {
      this.isAllowedCurrentLocation = true;
      navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        this.initMap(position.coords.latitude, position.coords.longitude)
        map.setCenter(pos);
      }, (error) => {
        if (error.code === 1) {
          this.isAllowedCurrentLocation = false;
        }
      });
    } else {
      this.isAllowedCurrentLocation = false;
      // Browser doesn't support Geolocation      
    }
  }

  initMap(latitude: number, longitude: number) {
    let latlng = new google.maps.LatLng(latitude, longitude);
    map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 15
    });

    const currentLocationMarker = new google.maps.Marker({
      map: map,
      position: latlng,
      animation: google.maps.Animation.DROP
    });

    currentLocationMarker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');

    infowindow = new google.maps.InfoWindow();
    this.loadingService.show();
    let service = new google.maps.places.PlacesService(map);
    service.textSearch({
      location: latlng,
      radius: '1000',
      query: 'comic stores near me'
    }, (result, status) => this.callback(result, status));
  }

  callback(results, status) {
    try {
      this.stores = [];
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        const bounds = new google.maps.LatLngBounds();
        for (let i = 0; i < results.length; i++) {
          let marker = new google.maps.Marker({
            map: map,
            position: results[i].geometry.location
          });

          google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(results[i].name);
            infowindow.open(map, this);
          });
          bounds.extend(results[i].geometry.location);
          this.stores.push({
            Address: results[i].formatted_address,
            Name: results[i].name
          } as ComicStore);
        }
        map.fitBounds(bounds);
      }
      this.loadingService.hide();
    } catch (error) {
      this.loadingService.hide();
    }
  }

}
