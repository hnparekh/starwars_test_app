import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, forkJoin, Observable } from 'rxjs';
import { StarWarsApiService, EventService } from 'src/app/core/services';
import { PaginationRequest } from 'src/app/models/pagination-request';
import { People, PaginationResponse, Starship, Species, Planet } from 'src/app/models/people';
import { LoadingService } from 'src/app/core/services/loading.service';
import { EventConstants } from 'src/app/shared/constants/event.constants';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit, OnDestroy {
  paginationRequest = new PaginationRequest();
  peopleResponse = new PaginationResponse<People>();
  filteredPeople: Array<People> = [];
  showPagination = false;
  subscription = new Subscription();

  constructor(
    private starWarsApiService: StarWarsApiService,
    private loadingService: LoadingService,
    private eventService: EventService
  ) { }

  ngOnInit(): void {
    this.getPeople(null);
    this.subscription.add(this.eventService.subscribe(EventConstants.filterStarship, (starships: Starship) => {
      /* There is no pagination structure in API to display filter result. so, we are displaying all Starships */
      const observables = [];
      starships.pilots.forEach(x => {
        observables.push(this.starWarsApiService.callUrl(x));
      });
      this.setPeople(observables);
    }));
    this.subscription.add(this.eventService.subscribe(EventConstants.filterSpecies, (species: Species) => {
      /* There is no pagination structure in API to display filter result. so, we are displaying all Species */
      const observables = [];
      species.people.forEach(x => {
        observables.push(this.starWarsApiService.callUrl(x));
      });
      this.setPeople(observables);
    }));
    this.subscription.add(this.eventService.subscribe(EventConstants.filterHomeworld, (planet: Planet) => {
      /* There is no pagination structure in API to display filter result. so, we are displaying all Planets */
      const observables = [];
      planet.residents.forEach(x => {
        observables.push(this.starWarsApiService.callUrl(x));
      });
      this.setPeople(observables);
    }));
  }

  setPeople(observables: any[]) {
    this.loadingService.show();
    this.subscription.add(forkJoin(observables).subscribe(x => {
      this.filteredPeople = [];
      this.peopleResponse.TotalRecords = x.length;
      this.peopleResponse.Result = x;
      this.showPagination = false;
      this.paginationRequest.PageSize = 0;
      this.loadingService.hide();
    }));
  }

  backtoAllPeople() {
    this.getPeople({ page: this.paginationRequest.PageNo });
  }

  /**
   * @returns void
   */
  getPeople(pagination: any | null): void {
    this.loadingService.show();
    this.paginationRequest.PageNo = !pagination ? 1 : pagination.page;
    this.peopleResponse.Result = !this.peopleResponse.Result ? [] : this.peopleResponse.Result;
    this.starWarsApiService.getPeople(this.paginationRequest).subscribe(response => {
      this.filteredPeople = [];
      this.peopleResponse.TotalRecords = response.TotalRecords;
      this.peopleResponse.Result = response.Result;
      this.showPagination = true;
      this.paginationRequest.PageSize = this.peopleResponse.Result.length;
      this.loadingService.hide();
    }, () => this.loadingService.hide());
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
