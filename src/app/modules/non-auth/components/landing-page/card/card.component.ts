import { Component, OnInit, OnDestroy, OnChanges, Input } from '@angular/core';
import { Subscription, Observable, forkJoin } from 'rxjs';
import { People, Species, Starship, Planet } from 'src/app/models/people';
import { StarWarsApiService, EventService } from 'src/app/core/services';
import { EventConstants } from 'src/app/shared/constants/event.constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnDestroy, OnChanges {

  subscription = new Subscription();
  @Input() people: People;
  species: Array<Species> = [];
  starships: Array<Starship> = [];
  planet: Planet;
  isFavourite = false;
  @Input() isReadOnlyCard = false;

  constructor(
    private starWarsApiService: StarWarsApiService,
    private eventService: EventService,
    private router: Router) { }

  ngOnInit() {
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes.people && changes.people.currentValue) {
      const people = this.starWarsApiService.getFavouritePeople();
      this.people = changes.people.currentValue;
      if (this.people) {
        this.isFavourite = people.some(x => x.name === this.people.name);
      }
      this.getSpecies();
      this.getStarShips();
      this.getHomeworld();
    }
    if (changes.isReadOnlyCard && changes.isReadOnlyCard.currentValue) {
      this.isReadOnlyCard = changes.isReadOnlyCard.currentValue;
    }

  }

  setFavourite() {
    if (!this.isReadOnlyCard) {
      this.isFavourite = !this.isFavourite;
      const people = this.starWarsApiService.getFavouritePeople();
      if (this.isFavourite) {
        people.push(this.people);
      } else {
        people.splice(people.findIndex(x => x.name === this.people.name), 1);
      }
      this.starWarsApiService.setFavouritePeople(people);
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * @returns void
   */
  getSpecies(): void {
    if (!this.people.species || this.people.species.length === 0) {
      this.species = [{ name: '-' } as Species];
    }
    const speciesObservables = [];
    this.people.species.forEach(x => {
      speciesObservables.push(this.starWarsApiService.callUrl(x));
    });

    forkJoin(speciesObservables).subscribe(x => {
      this.people.speciesNames = x;
      this.species = x;
    });
  }

  getStarShips(): void {
    if (!this.people.starships || this.people.starships.length === 0) {
      this.starships = [{ name: '-' } as Starship];
    }
    const starShipsObservables = [];
    this.people.starships.forEach(x => {
      starShipsObservables.push(this.starWarsApiService.callUrl(x));
    });

    forkJoin(starShipsObservables).subscribe(x => {
      this.people.ships = x;
      this.starships = x && x.length ? x : [{ name: '-' } as Starship];
    });
  }

  filterStarship(starship: Starship) {
    if (starship.name === '-') {
      return;
    }
    this.eventService.broadcast(EventConstants.filterStarship, starship);
  }

  filterHomeworld(planet: Planet) {
    if (planet.name === '-') {
      return;
    }
    this.eventService.broadcast(EventConstants.filterHomeworld, planet);
  }

  filterSpecies(species: Species) {
    if (species.name === '-') {
      return;
    }
    this.eventService.broadcast(EventConstants.filterSpecies, species);
  }

  getHomeworld(): void {
    if (!this.people.homeworld || this.people.homeworld.length === 0) {
      this.planet = {
        name: '-'
      } as Planet;
    }
    this.starWarsApiService.callUrl(this.people.homeworld).subscribe(x => {
      this.people.home = x.name;
      this.planet = x;
    });
  }

  navigateToDetails() {
    this.starWarsApiService.setPeopleValue = this.people;
    this.router.navigateByUrl('person');
  }


}
