import { Component, OnInit, OnDestroy } from '@angular/core';
import { PaginationResponse, People, Film, Planet, Starship } from 'src/app/models/people';
import { PaginationRequest } from 'src/app/models/pagination-request';
import { LoadingService } from 'src/app/core/services/loading.service';
import { StarWarsApiService } from 'src/app/core/services';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  peoplePaginationRequest = new PaginationRequest();
  peopleResponse = new PaginationResponse<People>();

  filmsPaginationRequest = new PaginationRequest();
  filmsResponse = new PaginationResponse<Film>();

  planetsPaginationRequest = new PaginationRequest();
  planetsResponse = new PaginationResponse<Planet>();

  spaceShipsPaginationRequest = new PaginationRequest();
  spaceShipsResponse = new PaginationResponse<Starship>();

  subscription = new Subscription();
  searchTerm: string;

  constructor(
    private starWarsApiService: StarWarsApiService,
    private loadingService: LoadingService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.subscription.add(this.route.params.subscribe(x => {
      this.searchTerm = x['term'];
      this.searchPeople(null);
      this.searchStarships(null);
      this.searchFilms(null);
      this.searchPlanets(null);
    }));
  }


  /**
   * @param  {any|null} pagination
   * @returns void
   */
  searchPeople(pagination: any | null): void {
    this.loadingService.show();
    this.peoplePaginationRequest.PageNo = !pagination ? 1 : pagination.page;
    this.peopleResponse.Result = !this.peopleResponse.Result ? [] : this.peopleResponse.Result;
    this.subscription.add(this.starWarsApiService.searchPeople(this.searchTerm, this.peoplePaginationRequest).subscribe(response => {
      this.peopleResponse.TotalRecords = response.TotalRecords;
      this.peopleResponse.Result = response.Result;
      this.peoplePaginationRequest.PageSize = this.peopleResponse.Result.length;
      this.loadingService.hide();
    }, () => this.loadingService.hide()));
  }

  /**
   * @param  {any|null} pagination
   * @returns void
   */
  searchStarships(pagination: any | null): void {
    this.loadingService.show();
    this.spaceShipsPaginationRequest.PageNo = !pagination ? 1 : pagination.page;
    this.spaceShipsResponse.Result = !this.spaceShipsResponse.Result ? [] : this.spaceShipsResponse.Result;
    this.subscription.add(this.starWarsApiService.searchSpacships(this.searchTerm, this.spaceShipsPaginationRequest).subscribe(response => {
      this.spaceShipsResponse.TotalRecords = response.TotalRecords;
      this.spaceShipsResponse.Result = response.Result;
      this.spaceShipsPaginationRequest.PageSize = this.spaceShipsResponse.Result.length;
      this.loadingService.hide();
    }, () => this.loadingService.hide()));
  }


  /**
   * @param  {any|null} pagination
   * @returns void
   */
  searchFilms(pagination: any | null): void {
    this.loadingService.show();
    this.filmsPaginationRequest.PageNo = !pagination ? 1 : pagination.page;
    this.filmsResponse.Result = !this.filmsResponse.Result ? [] : this.filmsResponse.Result;
    this.subscription.add(this.starWarsApiService.searchFilms(this.searchTerm, this.filmsPaginationRequest).subscribe(response => {
      this.filmsResponse.TotalRecords = response.TotalRecords;
      this.filmsResponse.Result = response.Result;
      this.spaceShipsPaginationRequest.PageSize = this.filmsResponse.Result.length;
      this.loadingService.hide();
    }, () => this.loadingService.hide()));
  }

  searchPlanets(pagination: any | null): void {
    this.loadingService.show();
    this.planetsPaginationRequest.PageNo = !pagination ? 1 : pagination.page;
    this.planetsResponse.Result = !this.planetsResponse.Result ? [] : this.planetsResponse.Result;
    this.subscription.add(this.starWarsApiService.searchPlanets(this.searchTerm, this.planetsPaginationRequest).subscribe(response => {
      this.planetsResponse.TotalRecords = response.TotalRecords;
      this.planetsResponse.Result = response.Result;
      this.planetsPaginationRequest.PageSize = this.planetsResponse.Result.length;
      this.loadingService.hide();
    }, () => this.loadingService.hide()));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
