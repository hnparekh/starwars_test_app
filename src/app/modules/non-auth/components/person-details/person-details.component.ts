import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { People, Film } from 'src/app/models/people';
import { StarWarsApiService } from 'src/app/core/services';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss']
})
export class PersonDetailsComponent implements OnInit {
  @Input() people: any;

  constructor(
    private router: Router,
    private starWarsApiService: StarWarsApiService
  ) {
  }

  ngOnInit() {
    if (!this.starWarsApiService.getPeopleValue) {
      this.goToLandingPage();
    } else {
      this.getFilms();
      this.people = this.starWarsApiService.getPeopleValue;
      this.getSpecies();
      this.getStarShips();
      this.getHomeworld();
    }
  }

  /**
  * @returns void
  */
  getSpecies(): void {
    const speciesObservables = [];
    this.people.species.forEach(x => {
      speciesObservables.push(this.starWarsApiService.callUrl(x));
    });

    forkJoin(speciesObservables).subscribe(x => {
      this.people.speciesNames = x;
    });
  }

  getStarShips(): void {
    const starShipsObservables = [];
    this.people.starships.forEach(x => {
      starShipsObservables.push(this.starWarsApiService.callUrl(x));
    });

    forkJoin(starShipsObservables).subscribe(x => {
      this.people.ships = x;
    });
  }


  getHomeworld(): void {
    this.starWarsApiService.callUrl(this.people.homeworld).subscribe(x => {
      this.people.home = x.name;
    });
  }

  goToLandingPage() {
    this.router.navigateByUrl('');
  }

  getFilms(): void {
    const filmsObservables = [];
    this.starWarsApiService.getPeopleValue.films.forEach(x => {
      filmsObservables.push(this.starWarsApiService.callUrl(x));
    });

    forkJoin(filmsObservables).subscribe(x => {
      this.people.filmNames = x.map(z => {
        return {
          title: z.title,
          description: z.opening_crawl,
          url: z.url
        } as Film;
      });
    });
  }

}
