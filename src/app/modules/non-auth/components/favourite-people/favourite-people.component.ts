import { Component, OnInit } from '@angular/core';
import { People } from 'src/app/models/people';
import { StarWarsApiService } from 'src/app/core/services';

@Component({
  selector: 'app-favourite-people',
  templateUrl: './favourite-people.component.html',
  styleUrls: ['./favourite-people.component.scss']
})
export class FavouritePeopleComponent implements OnInit {

  people: Array<People> = [];
  constructor(
    private starWarsApiService: StarWarsApiService
  ) { }

  ngOnInit() {
    this.people = this.starWarsApiService.getFavouritePeople();
  }

}
