import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NonAuthRoutingModule } from './non-auth-routing.module';
import { NonAuthLayoutComponent } from './non-auth-layout.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { StarWarsApiService } from 'src/app/core/services';
import { LandingPageComponent } from './components';
import { CardComponent } from './components/landing-page/card/card.component';
import { PersonDetailsComponent } from './components/person-details/person-details.component';
import { FavouritePeopleComponent } from './components/favourite-people/favourite-people.component';
import { ComicStoreComponent } from './components/comic-store/comic-store.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';

@NgModule({
  declarations: [
    NonAuthLayoutComponent,
    LandingPageComponent,
    CardComponent,
    PersonDetailsComponent,
    FavouritePeopleComponent,
    ComicStoreComponent,
    SearchResultsComponent
  ],
  imports: [
    CommonModule,
    NonAuthRoutingModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [StarWarsApiService]
})
export class NonAuthModule { }
