export class PaginationRequest {
    PageNo: number;
    PageSize: number;
}