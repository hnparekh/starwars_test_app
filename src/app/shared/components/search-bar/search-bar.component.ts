import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { People, Starship, Film } from 'src/app/models/people';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit, OnChanges {

  search = new FormControl('');
  people: Array<People> = [];
  planets = [];
  spaceships: Array<Starship> = [];
  films: Array<Film> = [];
  filterByUrls = [];

  constructor(
    private router: Router) { }

  @Output() filterBy = new EventEmitter<any>();
  @Input() showFilterBox: any;
  @Input() removeAllFilters: boolean;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.removeAllFilters && this.removeAllFilters) {
      this.filterByUrls = [];
    }
  }

  ngOnInit() {
  }

  onFavouriteClick() {
    this.router.navigateByUrl('/favourite-people');
  }

  findDistinct(urls: Array<string>) {
    const result = [];
    const map = new Map();
    for (const item of urls) {
      if (!map.has(item)) {
        map.set(item, true);    // set any value to Map
        result.push(item);
      }
    }
    return result;
  }

  // Search star war entities
  searchStarWar() {
    if (this.search.valid && !this.search.pristine) {
      if (this.search.value === '') {
        return;
      }
    }

    this.router.navigateByUrl('/search-results/' + this.search.value);
  }


}
