import { Injectable } from '@angular/core';
import { EventService } from './event.service';
import { EventConstants } from 'src/app/shared/constants/event.constants';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(private eventService: EventService) { }

  show() {
    this.eventService.broadcast(EventConstants.displayLoading, true);
  }

  hide() {
    this.eventService.broadcast(EventConstants.displayLoading, false);
  }
}
