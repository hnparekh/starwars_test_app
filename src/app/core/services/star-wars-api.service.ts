import { Injectable } from '@angular/core';
import { HttpClientService } from '../interceptors/http-client.service';
import { PaginationRequest } from 'src/app/models/pagination-request';
import { Film, Starship, Species, People, PaginationResponse, Planet } from 'src/app/models/people';
import { map, mergeMap } from 'rxjs/operators';
import { Observable, forkJoin } from 'rxjs';

@Injectable()

export class StarWarsApiService {
    private people: People;
    public get getPeopleValue(): People {
        return this.people;
    }
    public set setPeopleValue(people: People) {
        this.people = people;
    }

    getFavouritePeople(): Array<People> {
        return localStorage.getItem('favourite-people') ?
            JSON.parse(localStorage.getItem('favourite-people')) : [];
    }

    setFavouritePeople(people: Array<People>): void {
        localStorage.setItem('favourite-people', JSON.stringify(people));
    }

    constructor(private httpService: HttpClientService) { }

    callUrl(url: string): Observable<any> {
        return this.httpService.get(url);
    }

    getPeopleById(id: number): Observable<any> {
        return this.httpService.get(`people/${id}`);
    }

    searchPeople(value: string, paginationRequest: PaginationRequest) {
        return this.httpService.get(`people/?search=${value}&page=${paginationRequest.PageNo}`)
            .pipe(mergeMap(async (response: any) => {
                const paginationResponse = new PaginationResponse<People>();
                paginationResponse.Result = response.results;
                paginationResponse.TotalRecords = response.count;
                return paginationResponse;
            }));
    }

    searchFilms(value: string, paginationRequest: PaginationRequest) {
        return this.httpService.get(`films/?search=${value}&page=${paginationRequest.PageNo}`)
            .pipe(mergeMap(async (response: any) => {
                const paginationResponse = new PaginationResponse<Film>();
                paginationResponse.Result = response.results.map(x => {
                    return {
                        title: x.title,
                        description: x.opening_crawl
                    } as Film
                });
                paginationResponse.TotalRecords = response.count;
                return paginationResponse;
            }));
    }

    searchPlanets(value: string, paginationRequest: PaginationRequest) {
        return this.httpService.get(`planets/?search=${value}&page=${paginationRequest.PageNo}`)
            .pipe(mergeMap(async (response: any) => {
                const paginationResponse = new PaginationResponse<Planet>();
                paginationResponse.Result = response.results;
                paginationResponse.TotalRecords = response.count;
                return paginationResponse;
            }));
    }

    searchSpacships(value: string, paginationRequest: PaginationRequest) {
        return this.httpService.get(`starships/?search=${value}&page=${paginationRequest.PageNo}`)
            .pipe(mergeMap(async (response: any) => {
                const paginationResponse = new PaginationResponse<Starship>();
                paginationResponse.Result = response.results;
                paginationResponse.TotalRecords = response.count;
                return paginationResponse;
            }));
    }

    getPeople(paginationRequest: PaginationRequest) {
        return this.httpService.get(`people/?page=${paginationRequest.PageNo}`)
            .pipe(mergeMap(async (response: any) => {
                const paginationResponse = new PaginationResponse<People>();
                paginationResponse.Result = response.results;
                paginationResponse.TotalRecords = response.count;
                return paginationResponse;
            }));
    }
}
