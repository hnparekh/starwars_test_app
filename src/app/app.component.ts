import { Component, OnDestroy } from '@angular/core';
import { EventService } from './core/services';
import { Subscription } from 'rxjs';
import { EventConstants } from './shared/constants/event.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  isLoading: boolean;
  subscription = new Subscription();

  constructor(private eventService: EventService) {
    this.subscription.add(this.eventService.subscribe(EventConstants.displayLoading, (isLoading) => {
      this.isLoading = isLoading;
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
